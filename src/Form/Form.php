<?php
/*
 * (c) RICHARD Eddy <richard.eddy@live.fr>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Form;

/**
 * Class Form
 * @package Form
 */
class Form
{
    /**
     * Verify the token.
     * @param $form
     * @return bool
     */
    function verifyFormToken($form)
    {
        if(!isset($_SESSION[$form.'_token'])) {
            return false;
        }

        if(!isset($_POST['token'])) {
            return false;
        }

        if ($_SESSION[$form.'_token'] !== $_POST['token']) {
            return false;
        }

        return true;
    }

    /**
     * Generate the token.
     * @param $form
     * @return string
     */
    function generateFormToken($form)
    {
        $token = md5(uniqid(microtime(), true));
        $_SESSION[$form.'_token'] = $token;

        return $token;
    }
}
